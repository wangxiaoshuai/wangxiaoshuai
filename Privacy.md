## **来钱账本 Pririvacy Policy**

##### 首次更新日期：2023-12-12
****
#### 信息存储
- 本应用承诺不会存储和使用您的数据，您的数据将只存储在您的手机本地和iCloud中（如果您在手机设置中允许了本软件使用iCloud备份）。

#### 数据安全
- 请您妥善保管您的iCloud账号，不要随意泄露给他人，同时不要给你的设备做越狱处理，以保证您的数据是安全的的。

#### 联系我们
- 如果您有任何疑问，可以给我们发邮件到1398669315@qq.com，感谢您的信任与支持。